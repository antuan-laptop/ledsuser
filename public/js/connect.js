$(function(){
    $.fn.modal.defaults.spinner = $.fn.modalmanager.defaults.spinner =
    '<div class="loading-spinner" style="width: 200px; margin-left: -100px;">' +
        '<div class="progress progress-striped active">' +
            '<div class="progress-bar" style="width: 100%;"></div>' +
        '</div>' +
    '</div>';
});

var $modal = $('#ajax-modal');

$(document).on('click', '.registration', function(e){
    console.log(e);
    $('body').modalmanager('loading');
    setTimeout(function(){
        $modal.load('/leds-user/login', '', function(){
            $modal.modal();
        });
    }, 0);
});

$(document).on('click', '.load-register', function(e){
    $modal.modalmanager('loading');
    setTimeout(function(){
        $modal.load('/leds-register', '', function(){
            $modal.modal();
        });
    }, 0);
});

$modal.on('click', '.load-login', function(){
    $modal.modalmanager('loading');
    setTimeout(function(){
        $modal.load('/leds-user/login', '', function(){
            $modal.modal();
        });
    }, 0);
});
