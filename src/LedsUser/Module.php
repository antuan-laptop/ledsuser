<?php
namespace LedsUser;

use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;

class Module{

    public function onBootstrap(MvcEvent $e){
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new moduleRouteListener();
        $moduleRouteListener->attach($eventManager);
        // $eventManager->attach('dispatch', array($this, 'loadConfiguration'));
    }

    public function getServiceConfig() {
        return array(
			'factories' => array(
				'Zend\Session\Container' => function(){
					$container = new \Zend\Session\Container('local_auth');
					$manager = $container->getManager();
				},
                'Zend\Authentication\AuthenticationService' => function($sm) {
            		return $sm->get('doctrine.authenticationservice.orm_default');
        		},
                'leds_user_module' => function($sm) {
            		$em = $sm->get('doctrine.entitymanager.orm_default');
            		$authenticationService = $sm->get('Zend\Authentication\AuthenticationService');
            		return new \LedsUser\Service\UserService($em, $authenticationService);
        		},
                // 'sessionService' => function($sm) {
                //     return new LedsUser\Service\Session();
                // },
            )
        );
    }

    public function getControllerPluginConfig(){
        return array(
            'factories' => array(
                // 'session' => function($serviceLocator) {
                //     $sessionService = $serviceLocator->get('sessionService');
                //     return new LedsUser\Controller\Plugin\Session($sessionService);
                // },
            ),
        );
    }

    public function getViewHelperConfig(){
        return array(

        );
    }

    public function getConfig(){
        return include __DIR__ . '/../../config/module.config.php';
    }

    public function getAutoloaderConfig(){
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/../../src/' . __NAMESPACE__,
                ),
            ),
        );
    }

}
