<?php
namespace LedsUser\Repository;

use Doctrine\ORM\EntityRepository;
use LedsUser\Entity\User;

class UserRepository extends EntityRepository{

	public function create(User $user){
		$this->_em->persist($user);
		$this->_em->flush($user);
		return $user;
	}

	// public function saveToDb($data){
	// 	$em = $this->getEntityManager();
	// 	$user = new User();
	// 	$user->setName($data['name']);
	// 	$user->setEmail($data['email']);
	// 	$em->persist($user);
	// 	//$em->flush();
	// }

	public function updateDb($data){
		$em = $this->getEntityManager();
		$user = new User();
		$em->persist($user);
		$em->flush();
	}

	public function updateUser(User $user){
		$this->_em->persist($user);
		$this->_em->flush($user);
		return $user;
	}

	// public function getProfileId($id){
	// 	$em = $this->getEntityManager();
	// 	$profileId = $em->createQuery("SELECT n FROM User\Entity\Profile n WHERE n.id = :id");
	// 	$profileId->setParameter('id', $id);
	// 	$profileId->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
	// 	return $profileId->getArrayResult();
	// }

}
