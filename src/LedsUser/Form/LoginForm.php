<?php

namespace LedsUser\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use LedsUser\Entity\User;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class LoginForm extends Form {

    public function __construct() {
        parent::__construct('login');
        $this->setHydrator(new ClassMethodsHydrator(false))
                ->setObject(new User());

        //Méthode d'envoie (GET,POST)
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-signin');

        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'type' => 'email',
            'name' => 'email',
            'attributes' => array(
                'id' => 'email',
                'class' => 'form-control',
                'placeholder' => 'Email'
            ),
        ));

        $this->add(array(
            'type' => 'password',
            'name' => 'password',
            'attributes' => array(
                'id' => 'password',
                'class' => 'form-control',
                'placeholder' => 'Password'
            ),
        ));

        $csrf = new Element\Csrf('csrf');
        $this->add($csrf);

        $submitField = new Element\Submit('submit');
        $submitField->setValue('Validation');
        $submitField->setAttribute('class', 'btn btn-lg btn-primary btn-block');
        $submitField->setAttribute('id', 'submitbutton');
        $this->add($submitField);
    }

}
