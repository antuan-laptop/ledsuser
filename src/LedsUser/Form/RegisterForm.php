<?php
namespace LedsUser\Form;

use Zend\Form\Form;
use Zend\Form\Element;
use LedsUser\Entity\User;
use Zend\Stdlib\Hydrator\ClassMethods as ClassMethodsHydrator;

class RegisterForm extends Form {

    public function __construct() {

        parent::__construct('register');
        $this->setHydrator(new ClassMethodsHydrator(FALSE))
                ->setObject(new User());
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-signin');

        $idField = new Element\Hidden('id');
        $this->add($idField);

        $roleField = new Element\Hidden('role');
        $roleField->setValue('admin');
        $this->add($roleField);

        $this->add(array(
            'type' => 'text',
            'name' => 'displayName',
            'attributes' => array(
                'id' => 'displayName',
                'class' => 'form-control',
                'placeholder' => 'Display name'
            ),
        ));

        $this->add(array(
            'type' => 'email',
            'name' => 'email',
            'attributes' => array(
                'id' => 'email',
                'class' => 'form-control',
                'placeholder' => 'Email'
            ),
        ));

        $this->add(array(
            'type' => 'password',
            'name' => 'password',
            'attributes' => array(
                'id' => 'password',
                'class' => 'form-control',
                'placeholder' => 'Password'
            ),
        ));

        $this->add(array(
            'type' => 'password',
            'name' => 'confirmPassword',
            'attributes' => array(
                'id' => 'confirmPassword',
                'class' => 'form-control',
                'placeholder' => 'Confirm passwd'
            ),
        ));

        $this->add(array(
            'type' => 'captcha',
            'name' => 'captcha',
            'attributes' => array(
                'id' => 'captcha',
                'class' => 'form-control',
                'placeholder' => 'Human check'
            ),
            'options' => array(
//                'label' => 'Human Check',
                'captcha' => array(
                    'class' => 'Image',
                    'imgDir' => 'public/captcha',
                    'suffix' => '.png',
                    'imgUrl' => '/captcha/',
                    // 'imgAlt' => 'captcha img',
                    'font' => './data/font/artrbdo.ttf', 
                    'wordlen' => 5,
                    'fsize' => 28,
                    'width' => 250,
                    'height' => 50,
                    'expiration' => 600,
                    'dotNoiseLevel' => 0,
                    'lineNoiseLevel' => 0,
                ),
            ),
        ));

        $csrf = new Element\Csrf('csrf');
        $this->add($csrf);

        $submitField = new Element\Submit('submit');
        $submitField->setValue('Validation');
        $submitField->setAttribute('class', 'btn btn-lg btn-primary btn-block');
        $submitField->setAttribute('id', 'submitbutton');
        $this->add($submitField);
    }

}
