<?php

namespace LedsUser\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DoctrineModule\Validator\ObjectExists;
use Zend\Form\FormInterface;
use LedsUser\Form\RegisterForm;
use LedsUser\Entity\User;
use Zend\Crypt\Password\Bcrypt;
use Zend\Session\Container;
use Zend\Authentication\AuthenticationService;

class RegisterController extends AbstractActionController{

    protected $em;

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        }
        return $this->em;
    }

    public function indexAction() {

        $view = new ViewModel();
        $view->setTerminal(true);

        if ($this->identity()) {
            return $this->redirect()->toRoute('ledsUser');
        }
        $form = new RegisterForm();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $user = new User();
            $data = $form->setData($request->getPost());

            $mailfield = $request->getPost($user->getEmail());
            $validator = new ObjectExists(array(
                'object_repository' => $this->getEntityManager()->getRepository('LedsUser\Entity\User'),
                'fields' => 'email'
			));

			// Verify if user exist
            if ($validator->isValid($mailfield) === true) {
				return $this->redirect()->toRoute('ledsUser');
            }

            if ($form->isValid()) {
                $datas = $form->getData(FormInterface::VALUES_AS_ARRAY);
                $email = $request->getPost('email');
                $password = $request->getPost('password');
                $confirmPass = $request->getPost('confirmPassword');
                $displayname = $request->getPost('displayName');

                $user->setEmail($email);
                $hashpass = $this->createBcrypt($password); // create hash
                $user->setPassword($hashpass);
                $user->setDisplayname($displayname);

                $hash = $user->getPassword();
                $compare = $confirmPass;

                $bcrypt = new Bcrypt();
                if ($bcrypt->verify($compare, $hash)) {
                    $user->setConfirmpassword('NULL');
                    $this->getEntityManager()->persist($user);
                    $this->getEntityManager()->flush();
                } else {
                    echo 'not ok';
                }

				$userService = $this->getServiceLocator()->get('leds_user_module');
				$isAuthenticated = $userService->authenticate($mailfield['email'], $mailfield['password']);
                $auth = new AuthenticationService();

                if($auth->hasIdentity()) $identity = $auth->getIdentity();

                    $id = $identity['id'];
                    $container = new Container('local_auth');
                    $container->id = $id;
                    $container->email = $user->getEmail();
                    $container->cookie = $_COOKIE;

                    // $name = $this->formatName($username);

                    return $this->redirect()->toRoute('ledsUser');
            }
        }
        $view->setVariables(array('form' => $form));
        return $view;
    }

    public function createBcrypt($hashepass) {
        $bcrypt = new Bcrypt();
        return $bcrypt->create($hashepass);
    }


}
