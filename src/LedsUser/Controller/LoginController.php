<?php

namespace LedsUser\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use LedsUser\Form\LoginForm;
use LedsUser\Entity\User;
use Zend\Session\Container;
use Zend\Authentication\AuthenticationService;

class LoginController extends AbstractActionController{

    protected $em;

    public function getEntityManager() {
        if (null === $this->em) {
            $this->em = $this->getServiceLocator()->get('doctrine.entitymanager.orm_default');
        }
        return $this->em;
    }

    public function getUserRepository(){

		$em = $this->getEntityManager();
		return $em->getRepository('LedsUser\Entity\User');  
	}

    public function indexAction(){

        $refererContainer = new Container('referer');
		$redirect = $_SESSION['referer']['redirection'];
        // var_dump($redirect);

        if ($this->identity()) {
            return $this->redirect()->toRoute('ledsUser');
        }

        $form = new LoginForm();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $user = new User();

			$form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilter());

            $user->setEmail($this->getRequest()->getPost('email'));
            $user->setPassword($this->getRequest()->getPost('password'));

            $userService = $this->getServiceLocator()->get('leds_user_module');
            $isAuthenticated = $userService->authenticate($user->email, $user->password);
            $auth = new AuthenticationService();

            if($auth->hasIdentity()) $identity = $auth->getIdentity();

                $id = $identity['id'];
                $container = new Container('local_auth');
                $container->id = $id;
                $container->email = $user->getEmail();
                $container->cookie = $_COOKIE;

                return $this->redirect()->toRoute('ledsUser');
        }

        $view = new ViewModel();
        $view->setVariables(array(
            'error' => 'Your authentication credentials are not valid',
            'form' => $form
        ));
		$view->setTerminal(true);
		return $view;
    }

}
