<?php
namespace LedsUser\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;
use LedsUser\Service\Session;

class Session extends AbstractPlugin
{
    protected $sessionService;

    public function __construct(
        Session $sessionService
    ) {
        $this->sessionService = $sessionService;
    }

    public function getContainer() {
        return $this->sessionService->getContainer();
    }

    public function __invoke() {
        return $this->getContainer();
    }
}
