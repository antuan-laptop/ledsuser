<?php

namespace LedsUser\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class IndexController extends AbstractActionController{

    public function indexAction() {
        if (!$user = $this->identity()) {
            $this->redirect()->toRoute('home');
        } elseif ($user = $this->identity()) {
            $auth = $this->getServiceLocator()->get('leds_user_module');
			// if($user->getRole() == 'admin')
			// 	$this->redirect()->toRoute('admin');
		}
        //$user = $this->identity();
        return new ViewModel(array(
            'user' => $user
        ));
    }

    public function logoutAction() {
		$container = new Container();
		$sessionManager = $container->getManager();
		$sessionManager->getStorage()->clear();
		//var_dump($_SESSION); die;
		$userService = $this->getServiceLocator()->get('leds_user_module');
        $userService->logout();

        return $this->redirect()->toRoute('home');
    }
}
