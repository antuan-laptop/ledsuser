<?php
namespace LedsUser\Service;

use Zend\Crypt\Password\Bcrypt;
use Zend\Authentication\AuthenticationService;
use Doctrine\ORM\EntityManager;
use LedsUser\Entity\User;
//use User\Form\UserForm;

class UserService {

    protected $entityManager;
    protected $authenticationService;

    public function __construct(EntityManager $entityManager, AuthenticationService $authenticationService) {
        $this->authenticationService = $authenticationService;
        $this->entityManager = $entityManager;
    }

    public static function verify(User $user, $passwordGiven) {
        $BCrypt = new Bcrypt();
        return $BCrypt->verify($passwordGiven, $user->getPassword());
    }

    public function authenticate($email, $password) {
        $adapter = $this->authenticationService->getAdapter();
        $adapter->setIdentityValue($email);
        $adapter->setCredentialValue($password);
        $authResult = $this->authenticationService->authenticate();
        return $authResult->isValid();
    }

    public function hasIdentity() {
        $this->authenticationService->hasIdentity();
    }

    public function logout() {
        $this->authenticationService->clearIdentity();
    }

}
