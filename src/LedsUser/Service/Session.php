<?php
namespace LedsUser\Service;

use Zend\Session\Container;

class Session
{
    private static $container;

    public function getContainer() {
        if (!isset(self::$container)) {
            self::$container = new \Zend\Session\Container('local_auth');
        }
        return self::$container;
    }

    public function __invoke() {
        return $this->getContainer();
    }
}
