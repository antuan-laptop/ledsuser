Introduction
------------
This is an User module for authentication and Doctine2 for database setup for ZF2.

Installation
------------

Using Composer (recommended)
----------------------------
The recommended way to get a working copy of this project is to clone the repository
and use `composer` to install dependencies:

    curl -s https://getcomposer.org/installer | php --

You would then invoke `composer` to install dependencies. Add to your composer.json

	"antuan-laptop/ledsuser": "dev-master"

    (for needed classes)
    "antuan-laptop/commons": "dev-master"

Configuration
-------------

Once module installed, you could declare the module into your "config/application.config.php" by adding "LedsUser" and "Commons".

        'Application',
		'AkrabatSession',
        'DoctrineModule',
		'DoctrineORMModule',
		'AssetManager',
        'Commons',
        'LedsUser',

Setup Session Handling
----------------------

Create a directory "session" in data directory with read/write access to your web server

    $ mkdir www/{approot}/data/session
    $ chown -R apache:apache www/{approot}/data/session
    $ chmod -R 0770 apache:apache www/{approot}/data/session

	return array(
		'session' => array(
			'name' => 'local_auth',
			'save_path' => __DIR__ . '/../../../data/session'
		),
	);

Database Setup
--------------
copy "doctrine.local.php.dist" to "config/autoload/doctrine.local.php"

	./vendor/bin/doctrine-module orm:schema-tool:update --force

Forking repository
------------------
