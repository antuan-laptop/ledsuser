<?php
return array(
	'session' => array(
		'name' => 'local_auth',
		'save_path' => __DIR__ . '/../../../data/session'
	),
    'asset_manager' => array(
        'resolver_configs' => array(
            'paths' => array(
                'leds-user' => __DIR__ . '/../public',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'LedsUser\Controller\Index' => 'LedsUser\Controller\IndexController',
            'LedsUser\Controller\Register' => 'LedsUser\Controller\RegisterController',
            'LedsUser\Controller\Login' => 'LedsUser\Controller\LoginController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'ledsUser' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/leds-user[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'LedsUser\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'ledsLogin' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/leds-user/login',
                    'defaults' => array(
                        'controller' => 'LedsUser\Controller\Login',
                        'action' => 'index',
                    ),
                ),
            ),
            'ledsRegister' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/leds-register[/:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'LedsUser\Controller\Register',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'doctrine' => array(
        'driver' => array(
            'LedsUser_driver' => array(
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver', 
                'cache' => 'array',
                'paths' => array(__DIR__ . '/../src/LedsUser/Entity')
            ),
            'orm_default' => array(
                'drivers' => array(
                    'LedsUser\Entity' => 'LedsUser_driver'
                )
            )
        ),
        'authentication' => array(
            'orm_default' => array(
                'object_manager' => 'Doctrine\ORM\EntityManager',
                'identity_class' => 'LedsUser\Entity\User',
                'identity_property' => 'email',
                'credential_property' => 'password',
                'credential_callable' => function(\LedsUser\Entity\User $user, $passwordGiven) {
            return \LedsUser\Service\UserService::verify($user, $passwordGiven);
        },
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
        'template_map'=> array(
    		'leds-user/layout' => __DIR__ . '/../view/layout/layout.phtml',
    		'loggedin' => __DIR__ . '/../view/partials/loggedin.phtml',
    		// 'leds-user/index/index' => __DIR__ . '/../view/leds-user/index/index.phtml',
		),
    ),
);
